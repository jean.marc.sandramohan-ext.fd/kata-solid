import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

export class Universe {
}

@Injectable({
  providedIn: 'root'
})
export class UniverseService {
  getUniverses(): Observable<Universe[]> {
    return new Observable<Universe[]>();
  }
  addUniverse(item: Universe): Observable<Universe> {
    return new Observable<Universe>();
  }
  updateUniverse(item: Universe): Observable<Universe> {
    return new Observable<Universe>();
  }
  deleteUniverse(id: number): Observable<boolean> {
    return new Observable<boolean>();
  }
}

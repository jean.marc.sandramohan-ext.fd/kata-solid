import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getUsers() {
    return [{
      name: "John",
      age: 30,
      email: "john@gmail.com",
      address : "1 rue laplace",
      city: "Paris",
      state: "Ile de France",
    }, {
      name: "Peter",
      age: 40,
      email: "peter@gmail.com",
      address : "2 rue laplace",
      city: "Paris",
      state: "Ile de France"
    }];
  }
}

import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RubricService {

  constructor(private rubricRepository: RubricRepository) {
  }

  getRubrics(): Observable<Rubric[]> {
    return this.rubricRepository.getRubrics();
  }
}

export class Rubric {
  id: number;
  name: string;
  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}

@Injectable({
  providedIn: 'root'
})
export class RubricRepository {
  private rubrics: Rubric[] = [
    { id: 1, name: 'Rubric 1' },
    { id: 2, name: 'Rubric 2' },
    { id: 3, name: 'Rubric 3' }
  ];

  getRubrics(): Observable<Rubric[]> {
    return of(this.rubrics);
  }
}

import { Injectable } from '@angular/core';

const ELECTRONICS_PRODUCT_1 = "Electronics --> Product 1";
const ELECTRONICS_PRODUCT_2 = "Electronics --> Product 2";
const CLOTHES_PRODUCT_1 = "Clothes --> Product 3";
const CLOTHES_PRODUCT_2 = "Clothes --> Product 4";
const GROCERIES_PRODUCT_1 = "Groceries --> Product 5";
const GROCERIES_PRODUCT_2 = "Groceries --> Product 6";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private INIT_PRODUCTS =
    [
      ELECTRONICS_PRODUCT_1,
      ELECTRONICS_PRODUCT_2,
      CLOTHES_PRODUCT_1,
      CLOTHES_PRODUCT_2,
      GROCERIES_PRODUCT_1,
      GROCERIES_PRODUCT_2
    ];
  constructor() { }

  getProducts() {
    return this.INIT_PRODUCTS;
  }

  getCategories() {
    return ["Electronics", "Clothes", "Groceries"];
  }

  getProductsByCategory(category: string) {
    switch (category) {
      case "Electronics":
        return [ELECTRONICS_PRODUCT_1, ELECTRONICS_PRODUCT_2];
      case "Clothes":
        return [CLOTHES_PRODUCT_1, CLOTHES_PRODUCT_2];
      case "Groceries":
        return [GROCERIES_PRODUCT_1, GROCERIES_PRODUCT_2];
      default:
        return [ELECTRONICS_PRODUCT_1, ELECTRONICS_PRODUCT_2,
          CLOTHES_PRODUCT_1, CLOTHES_PRODUCT_2,
          GROCERIES_PRODUCT_1, GROCERIES_PRODUCT_2];
    }
  }
}

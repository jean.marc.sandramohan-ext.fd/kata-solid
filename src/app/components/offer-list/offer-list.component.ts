import { Component } from '@angular/core';
import {NgForOf} from "@angular/common";

@Component({
  selector: 'app-offer-list',
  standalone: true,
    imports: [
        NgForOf
    ],
  templateUrl: './offer-list.component.html',
  styleUrl: './offer-list.component.css'
})
export class OfferListComponent {
  private dartyOffer: DartyOffer;
  private mkpOffer: DartyOffer;
  private fnacOffer: DartyOffer;

  constructor() {
    this.dartyOffer = new DartyOffer();
    this.mkpOffer = new MkpOffer();
    this.fnacOffer = new FnacOffer();
  }

  getOffers() {
    return [
      {
        offer: "Offre Darty dans une offre Darty",
        type: new DartyOffer().type()
      },
      {
        offer: "Offre Mkp dans une offre Mkp",
        type: new MkpOffer().type()
      },
      {
        offer: "Offre Fnac dans une offre Fnac",
        type: new FnacOffer().type()
      },
      {
        offer: "Offre Darty dans une offre Darty",
        type: this.dartyOffer.type()
      },
      {
        offer: "Offre Mkp dans une offre Darty",
        type: this.mkpOffer.type()
      },
      {
        offer: "Offre Fnac dans une offre Darty",
        type: this.fnacOffer.type()
      }
    ];
  }
}

class DartyOffer {
  type() : string {
    return "Darty";
  }
}

class MkpOffer extends DartyOffer {
  override type() : string {
    return "Mkp";
  }
}

class FnacOffer extends DartyOffer {
  override type() : string {
    return "Fnac";
  }
}


import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OfferListComponent } from './offer-list.component';

describe('OfferListComponent', () => {
  let component: OfferListComponent;
  let fixture: ComponentFixture<OfferListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OfferListComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(OfferListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get offers correctly', () => {
    const offers = component.getOffers();
    expect(offers.length).toBe(6);
    expect(offers[0].type).toBe('Darty');
    expect(offers[1].type).toBe('Mkp');
    expect(offers[2].type).toBe('Fnac');
    expect(offers[3].type).toBe('Darty');
    expect(offers[4].type).toBe('Darty');
    expect(offers[5].type).toBe('Darty');
  });
});

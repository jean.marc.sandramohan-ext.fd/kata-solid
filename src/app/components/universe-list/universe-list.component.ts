import { Component } from '@angular/core';
import {Universe, UniverseService} from "../../services/universe.service";

@Component({
  selector: 'app-universe-list',
  standalone: true,
  imports: [],
  templateUrl: './universe-list.component.html',
  styleUrl: './universe-list.component.css'
})
export class UniverseListComponent {
  universes: Universe[] = [];

  constructor(private universeService: UniverseService) {
  }

  ngOnInit() {
    this.universeService.getUniverses()
      .subscribe(universes => this.universes = universes);
  }
}

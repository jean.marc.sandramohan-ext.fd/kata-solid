import { Component } from '@angular/core';
import {UserService} from "../../services/user.service";
import {NgForOf} from "@angular/common";

@Component({
  selector: 'app-user-list',
  standalone: true,
  imports: [
    NgForOf
  ],
  templateUrl: './user-list.component.html',
  styleUrl: './user-list.component.css'
})
export class UserListComponent {
  users: any[];

  constructor(private userService: UserService) {
    this.users = this.userService.getUsers();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ProductListComponent } from './product-list.component';

describe('ProductListComponent', () => {
  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, ProductListComponent],
      declarations: [],
      providers: []
    })
      .compileComponents();

    fixture = TestBed.createComponent(ProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should filter products by category correctly', () => {
    component.filterByCategory("Electronics");
    expect(component.products).toEqual(["Electronics --> Product 1", "Electronics --> Product 2"]);
    expect(component.selectedCategory).toEqual("Electronics");
  });
});

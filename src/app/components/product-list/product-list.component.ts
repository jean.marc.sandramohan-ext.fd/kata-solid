import {Component} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {NgForOf} from "@angular/common";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-product-list',
  standalone: true,
  imports: [
    NgForOf,
    FormsModule
  ],
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css'
})
export class ProductListComponent{

  products: any[];
  categories: string[];
  selectedCategory: string;

  constructor(private productService: ProductService) {
    this.products = this.productService.getProducts();
    this.categories = this.productService.getCategories();
    this.selectedCategory = "";
  }

  filterByCategory(category: string) {
    this.selectedCategory = category;
    this.products = this.productService.getProductsByCategory(category);
  }
}

import { Component } from '@angular/core';
import {NgForOf} from "@angular/common";
import {Rubric, RubricService} from "../../services/rubric.service";

@Component({
  selector: 'app-rubric-list',
  standalone: true,
    imports: [
        NgForOf
    ],
  templateUrl: './rubric-list.component.html',
  styleUrl: './rubric-list.component.css'
})
export class RubricListComponent {
  rubrics: Rubric[] = [];

  constructor(private rubricService: RubricService) {
    this.rubricService
      .getRubrics()
      .subscribe(rubrics => this.rubrics = rubrics);
  }
}

import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {ProductListComponent} from "./components/product-list/product-list.component";
import {UserListComponent} from "./components/user-list/user-list.component";
import {OfferListComponent} from "./components/offer-list/offer-list.component";
import {UniverseListComponent} from "./components/universe-list/universe-list.component";
import {RubricListComponent} from "./components/rubric-list/rubric-list.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ProductListComponent, UserListComponent, OfferListComponent, UniverseListComponent, RubricListComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'kata-solid';
}
